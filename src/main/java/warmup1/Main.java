package warmup1;

public class Main {

    public static void main(String[] args) {

        Main main = new Main();
        System.out.println(main.missingChar("kitten", 5));
        System.out.println(main.missingChar("kitten", 0));
        System.out.println(main.missingChar("kitten", 1));

        System.out.println(main.frontBack("eodc"));
        System.out.println(main.frontBack("a"));
        System.out.println(main.frontBack("ab"));

        System.out.println(3 % 10);

        System.out.println(main.icyHot(-1, 101));
        System.out.println(main.icyHot(101, -2));
        System.out.println(main.icyHot(120, 12));

        System.out.println(main.endUp("hi there"));

        System.out.println(main.posNeg(1, -1, true));
    }

    public boolean sleepIn(boolean weekday, boolean vacation) {
        return vacation || !weekday;
    }

    public boolean monkeyTrouble(boolean aSmile, boolean bSmile) {
        return aSmile == bSmile;
    }

    public int sumDouble(int a, int b) {
        if (a == b) {
            return 2 * (a + b);
        } else {
            return a + b;
        }
    }

    public int diff21(int n) {
        if (n <= 21) {
            return Math.abs(21 - n);
        } else {
            return 2 * Math.abs(21 - n);
        }
    }


    public boolean parrotTrouble(boolean talking, int hour) {
        return talking && (hour < 7 || hour > 20);
    }

    public boolean makes10(int a, int b) {
        return a+b==10 || a==10 || b==10;
    }

    public boolean nearHundred(int n) {
        return Math.abs(100-n)<=10 || Math.abs(200-n)<=10;
    }

    public boolean posNeg(int a, int b, boolean negative) {
        if(!negative && ((a<0 && b>0) || (a>0 && b<0)))
            return true;
        else if (negative && a <0 && b<0)
            return true;
        return false;
    }


    public String missingChar(String str, int n) {
        if (n == 0) {
            return str.substring(1, str.length());
        } else if (n == str.length()) {
            return str.substring(0, str.length());
        } else {
            return str.substring(0, n) + str.substring(n + 1, str.length());
        }

    }

    public String frontBack(String str) {


        if (str.length() > 1) {
            char firstCharOfString = str.charAt(0);
            char lastCharOfString = str.charAt(str.length() - 1);
            return lastCharOfString + str.substring(1, str.length() - 1) + firstCharOfString;
        } else {
            return str;
        }
    }


    public String front3(String str) {
        if (str.length() <= 3) {
            return str + str + str;
        } else {
            str = str.substring(0, 3);
            return str + str + str;
        }
    }

    public String backAround(String str) {
        char lastLetterOfString = str.charAt(str.length() - 1);
        return lastLetterOfString + str + lastLetterOfString;
    }

    public boolean or35(int n) {
        return (n % 3 == 0 || n % 5 == 0);
    }

    public String front22(String str) {
        String front;
        if (str.length() < 2) {
            front = str;
        } else {
            front = str.substring(0, 2);
        }
        return front + str + front;
    }

    public boolean startHi(String str) {
        return str.startsWith("hi");
    }

    public boolean icyHot(int temp1, int temp2) {
        return ((temp1 < 0 && temp2 > 100) || (temp1 > 100 && temp2 < 0));
    }

    public boolean in1020(int a, int b) {
        return ((a >= 10 && a <= 20) || (b >= 10 && b <= 20));
    }

    public boolean hasTeen(int a, int b, int c) {
        return ((a >= 13 && a <= 19) || (b >= 13 && b <= 19) || (c >= 13 && c <= 19));
    }

    public boolean loneTeen(int a, int b) {
        return (((a >= 13 && a <= 19) && (b < 13 || b > 19)) ||
                ((b >= 13 && b <= 19) && (a < 13 || a > 19)));
    }

    public String delDel(String str) {
        if (str.length() < 4) {
            return str;
        } else {
            if (str.substring(1, 4).equals("del")) {
                if (str.length() == 4)
                    return String.valueOf(str.charAt(0));
                else
                    return str.charAt(0) + str.substring(4, str.length());
            } else {
                return str;
            }
        }
    }

    public boolean mixStart(String str) {
        if (str.length() >= 3) {
            return str.substring(1, 3).equals("ix");
        } else {
            return false;
        }
    }

    public String startOz(String str) {
        String firstLetterIsO = "";
        String secondLetterIsZ = "";
        if (str.length() >= 1) {
            if (str.charAt(0) == 'o') {
                firstLetterIsO = "o";
            }
        }
        if (str.length() >= 2) {
            if (str.charAt(1) == 'z') {
                secondLetterIsZ = "z";
            }
        }
        return firstLetterIsO + secondLetterIsZ;
    }

    public int close10(int a, int b) {
        if (Math.abs(10 - a) == Math.abs(10 - b)) {
            return 0;
        } else {
            if (Math.abs(10 - a) < Math.abs(10 - b)) {
                return a;
            } else
                return b;
        }
    }

    public boolean in3050(int a, int b) {
        if ((a >= 30 && a <= 40) && (b >= 30 && b <= 40)) {
            return true;
        }
        if ((a >= 40 && a <= 50) && (b >= 40 && b <= 50)) {
            return true;
        }
        return false;
    }

    public int max1020(int a, int b) {
        if ((a >= 10 && a <= 20) && (b >= 10 && b <= 20)) {
            if (a > b) {
                return a;
            } else {
                return b;
            }
        } else if ((a >= 10 && a <= 20) && (b < 10 || b > 20)) {
            return a;
        } else if ((b >= 10 && b <= 20) && (a < 10 || a > 20)) {
            return b;
        } else {
            return 0;
        }
    }

    public boolean stringE(String str) {
        int eCharCounter = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'e') {
                eCharCounter++;
            }
        }
        if (eCharCounter <= 3 && eCharCounter >= 1)
            return true;
        else
            return false;
    }

    public boolean lastDigit(int a, int b) {
        return (a % 10 == b % 10);
    }

    public String endUp(String str) {
        if (str.length() <= 3) {
            return str.toUpperCase();
        } else {
            String upperString = str.substring(str.length() - 3);
            return str.substring(0, str.length() - 4) + upperString.toUpperCase();
        }
    }

    public String everyNth(String str, int n) {
        if (str.length() == 0) {
            return "";
        } else {
            String result = "";
            for (int i = 0; i < str.length(); i = i + n) {
                result += String.valueOf(str.charAt(i));
            }
            return result;
        }
    }

}