package string1;

public class Exercises {

    public static void main(String[] args) {
        Exercises exercises = new Exercises();
        System.out.println(exercises.hasBad("xxbadxx"));
        System.out.println(exercises.lastChars("k", "zip"));
        System.out.println(exercises.frontAgain("edited"));
        System.out.println(exercises.withoutX2("xxHi"));
    }

    public String makeTags(String tag, String word) {
        return "<" + tag + ">" + word + "</" + tag + ">";
    }

    public String makeOutWord(String out, String word) {
        return out.substring(0, 2) + word + out.substring(2, out.length());
    }

    public String extraEnd(String str) {
        String twoLastCharsOfInputString = str.substring(str.length() - 2, str.length());
        return twoLastCharsOfInputString + twoLastCharsOfInputString + twoLastCharsOfInputString;
    }

    public String firstTwo(String str) {
        if (str.length() < 2) {
            return str;
        } else {
            return str.substring(0, 2);
        }
    }

    public String comboString(String a, String b) {
        if (a.length() < b.length()) {
            return a + b + a;
        } else {
            return b + a + b;
        }
    }

    public String left2(String str) {
        if (str.length() < 3) {
            return str;
        } else {
            String firstTwoChars = str.substring(0, 2);
            return str.substring(2) + firstTwoChars;
        }
    }

    public String right2(String str) {
        return str.substring(str.length() - 2) + str.substring(0, str.length() - 2);
    }

    public String withouEnd2(String str) {
        if (str.length() < 3) {
            return "";
        } else {
            return str.substring(1, str.length() - 1);
        }
    }

    public String middleTwo(String str) {
        return str.substring(str.length() / 2 - 1, str.length() / 2 + 1);
    }

    public boolean endsLy(String str) {
        return str.endsWith("ly");
    }

    public String nTwice(String str, int n) {
        return str.substring(0, n) + str.substring(str.length() - n);
    }

    public String twoChar(String str, int index) {
        if (str.length() < index + 2 || index < 0) {
            return str.substring(0, 2);
        } else {
            return str.substring(index, index + 2);
        }
    }

    public boolean hasBad(String str) {
        return str.matches("(.?)bad(.*)");
    }

    public String atFirst(String str) {
        String result;
        if (str.length() < 2) {
            result = str;
            for (int i = str.length(); i < 2; i++) {
                result += "@";
            }
        } else {
            result = str.substring(0, 2);
        }
        return result;
    }

    public String lastChars(String a, String b) {
        String result;
        if (a.length() == 0) {
            result = "@";
        } else {
            result = String.valueOf(a.charAt(0));
        }
        if (b.length() == 0) {
            result += "@";
        } else {
            result += b.charAt(b.length() - 1);
        }
        return result;

    }

    public String conCat(String a, String b) {
        String result = "";
        if (a.length() == 0) {
            result = b;
        } else if (b.length() == 0) {
            result = a;
        } else if (a.charAt(a.length() - 1) == b.charAt(0)) {
            if (b.length() > 1) {
                result = a + b.substring(1);
            } else {
                result = a;
            }
        } else {
            result = a + b;
        }
        return result;
    }

    public String lastTwo(String str) {
        if (str.length() < 2) {
            return str;
        } else {
            return str.substring(0, str.length() - 2) + str.charAt(str.length() - 1) + str.charAt(str.length() - 2);
        }
    }

    public String seeColor(String str) {
        if (str.matches("red.*")) {
            return "red";
        } else if (str.matches("blue.*")) {
            return "blue";
        } else {
            return "";
        }
    }

    public boolean frontAgain(String str) {
        boolean result = false;
        if (str.length() >= 2) {
            if (str.substring(0, 2).equals(str.substring(str.length() - 2))) {
                result = true;
            }
        }
        return result;
    }

    public String minCat(String a, String b) {
        if (a.length() == 0 || b.length() == 0) {
            return "";
        } else if (a.length() == b.length()) {
            return a + b;
        } else if (a.length() < b.length()) {
            return a + b.substring(b.length() - a.length());
        } else {
            return a.substring(a.length() - b.length()) + b;
        }
    }

    public String extraFront(String str) {
        if (str.length() < 2) {
            return str + str + str;
        } else {
            return str.substring(0, 2) + str.substring(0, 2) + str.substring(0, 2);
        }
    }

    public String without2(String str) {
        String result;
        if (str.length() >= 2) {
            if (str.substring(0, 2).equals(str.substring(str.length() - 2, str.length()))) {
                result = str.substring(2);
            } else {
                result = str;
            }
        } else {
            result = str;
        }
        return result;
    }

    public String deFront(String str) {
        String result;

        if (str.length() <= 1) {
            if (str.startsWith("a")) {
                return "";
            }
        }
        if (str.length() == 2) {
            if (str.startsWith("ab")) {
                return "ab";
            } else if (str.charAt(0) == 'a') {
                return "a";
            } else if (str.charAt(1) == 'b') {
                return "b";
            } else {
                return "";
            }
        } else {
            if (str.startsWith("ab")) {
                return str;
            } else if (str.charAt(0) == 'a') {
                return 'a' + str.substring(2);
            } else if (str.charAt(1) == 'b') {
                return str.substring(1);
            } else {
                return str.substring(2);
            }
        }
    }


    public String startWord(String str, String word) {
        if(str.length()<word.length()){
            return "";
        }else{
            if(str.substring(1, word.length()).equals(word.substring(1))){
                return str.substring(0,word.length());
            }else {
                return "";
            }
        }
    }

    public String withoutX(String str) {
        String result = str;
        if(str.startsWith("x")){
            result = result.substring(1);
        }
        if (result.endsWith("x")){
            result = result.substring(0, result.length()-1);
        }
        return result;
    }

    public String withoutX2(String str) {
        StringBuilder stringBuilder = new StringBuilder(str);

        if(str.length()>1) {
            if (stringBuilder.charAt(1) == 'x') {
                stringBuilder = stringBuilder.deleteCharAt(1);
            }
        }

        if(str.length()>0) {
            if (stringBuilder.charAt(0) == 'x') {
                stringBuilder = stringBuilder.deleteCharAt(0);
            }
        }else{
            return "";
        }

        return stringBuilder.toString();
    }


}
