package warmup2;

public class Warm2 {

    public static void main(String[] args) {
        Warm2 warm2 = new Warm2();
        System.out.println(warm2.stringX("xabxxxcdx"));
    }




    boolean doubleX(String str) {
        int indexOfFirstX = str.indexOf('x');
        int indexOfCharAfterFirstX = indexOfFirstX + 1;
        if (indexOfCharAfterFirstX < str.length() && str.charAt(indexOfCharAfterFirstX) == 'x') {
            return true;
        } else {
            return false;
        }
    }

    public String stringBits(String str) {
        String result = "";
        for (int i = 0; i < str.length(); i = i + 2) {
            result += str.charAt(i);
        }
        return result;
    }

    public String stringSplosion(String str) {
        String result = "";
        for (int i = 0; i < str.length() + 1; i++) {
            result += str.substring(0, i);
        }
        return result;
    }

    public int last2(String str) {
        if (str.length() <= 2) {
            return 0;
        } else {
            int counter = 0;
            String substringofLast2Elements = str.substring(str.length() - 2, str.length());
            for (int i = 0; i < str.length() - 2; i++) {
                if (str.substring(i, i + 2).equals(substringofLast2Elements)) {
                    counter++;
                }
            }
            return counter;
        }
    }

    public int arrayCount9(int[] nums) {
        int counter = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 9) {
                counter++;
            }
        }
        return counter;
    }

    public boolean arrayFront9(int[] nums) {
        boolean isNineAtOneOfFirst4Elements = false;
        for (int i = 0; i < nums.length && i < 4; i++) {
            if (nums[i] == 9) {
                isNineAtOneOfFirst4Elements = true;
            }
        }
        return isNineAtOneOfFirst4Elements;
    }

    public boolean array123(int[] nums) {
        boolean is123SequenceInsideArray = false;
        for (int i = 0; i < nums.length - 2; i++) {
            if (nums[i] == 1 && nums[i + 1] == 2 && nums[i + 2] == 3) {
                is123SequenceInsideArray = true;
            }
        }
        return is123SequenceInsideArray;
    }

    public int stringMatch(String a, String b) {
        int counter = 0;
        for (int i = 0; i + 2 <= a.length() && i + 2 <= b.length(); i++) {
            if (a.substring(i, i + 2).equals(b.substring(i, i + 2))) {
                counter++;
            }
        }
        return counter;
    }

    public String stringX(String str) {

        if (str.length() <= 2) {
            return str;
        } else {
            char firstChar = str.charAt(0);
            char lastChar = 0;
            String middle = "";

            lastChar = str.charAt(str.length() - 1);
            middle = str.substring(1, str.length() - 1);
            middle = middle.replaceAll("x", "");
            return firstChar + middle + lastChar;
        }
    }

    public String altPairs(String str) {
        int i = 0;
        String result = "";
        boolean isSecondInPair = false;
        while (i < str.length()) {
            result += str.charAt(i);
            if (!isSecondInPair) {
                i++;
                isSecondInPair = true;
            } else {
                i += 3;
                isSecondInPair = false;
            }
        }
        return result;
    }

    public String stringYak(String str) {
        return str.replaceAll("y.k", "");
    }

    public int array667(int[] nums) {
        int counter = 0;
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == 6 && (nums[i + 1] == 6 || nums[i + 1] == 7)) {
                counter++;
            }
        }
        return counter;
    }

    public boolean noTriples(int[] nums) {
        boolean isTriple = false;
        for (int i = 0; i < nums.length - 2; i++) {
            if (nums[i] == nums[i + 1] && nums[i + 1] == nums[i + 2]) {
                isTriple = true;
            }
        }
        return !isTriple;
    }

    public boolean has271(int[] nums) {
        boolean result = false;
        for (int i = 0; i < nums.length - 2; i++) {
            if (nums[i + 1] == nums[i] + 5 && nums[i + 2] == nums[i] - 1) {
                result = true;
            } else if (nums[i + 1] == nums[i] + 5 && Math.abs(nums[i + 2] - (nums[i] - 1)) <= 2) {
                result = true;
            }

        }
        return result;
    }
}
