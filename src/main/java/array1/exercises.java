package array1;

public class exercises {


    public boolean firstLast6(int[] nums) {
        return nums[0] == 6 || nums[nums.length - 1] == 6;
    }

    public boolean sameFirstLast(int[] nums) {
        return nums.length >= 1 && nums[0] == nums[nums.length - 1];
    }

    public int[] makePi() {
        return new int[]{3, 1, 4};
    }

    public boolean commonEnd(int[] a, int[] b) {
        return a[0] == b[0] || a[a.length - 1] == b[b.length - 1];
    }

    public int sum3(int[] nums) {
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        return sum;
    }

    public int[] rotateLeft3(int[] nums) {
        return new int[]{nums[1], nums[2], nums[0]};
    }

    public int[] reverse3(int[] nums) {
        return new int[]{nums[2], nums[1], nums[0]};
    }

    public int[] maxEnd3(int[] nums) {
        int max = nums[0];
        if (max < nums[nums.length - 1]) {
            max = nums[nums.length - 1];
        }

        return new int[]{max, max, max};
    }

    public int sum2(int[] nums) {
        if (nums.length == 0) {
            return 0;
        } else if (nums.length == 1) {
            return nums[0];
        } else {
            return nums[0] + nums[1];
        }
    }

    public int[] middleWay(int[] a, int[] b) {
        return new int[]{a[1], b[1]};
    }

    public int[] makeEnds(int[] nums) {
        return new int[]{nums[0], nums[nums.length - 1]};
    }

    public boolean has23(int[] nums) {
        return nums[0] == 2 || nums[1] == 2 || nums[0] == 3 || nums[1] == 3;
    }

    public boolean no23(int[] nums) {
        return nums[0] != 2 && nums[1] != 2 && nums[0] != 3 && nums[1] != 3;
    }

    public int[] makeLast(int[] nums) {
        int[] result = new int[nums.length * 2];
        result[result.length - 1] = nums[nums.length - 1];
        return result;
    }

    public boolean double23(int[] nums) {
        boolean result = false;
        if (nums.length > 1) {
            if (nums[0] == 2 && nums[1] == 2 || nums[0] == 3 && nums[1] == 3) {
                result = true;
            }
        }
        return result;
    }

    public int[] fix23(int[] nums) {

        if (nums[0] == 2 && nums[1] == 3) {
            return new int[]{nums[0], 0, nums[2]};
        } else if (nums[1] == 2 && nums[2] == 3) {
            return new int[]{nums[0], nums[1], 0};
        } else {
            return nums;
        }
    }

    public int start1(int[] a, int[] b) {
        int counter = 0;
        if (a.length > 0 && a[0] == 1) {
            counter++;
        }
        if (b.length > 0 && b[0] == 1) {
            counter++;
        }
        return counter;
    }

    public int[] biggerTwo(int[] a, int[] b) {
        int sumOfA = 0;
        int sumOfB = 0;

        sumOfA = a[0] + a[1];
        sumOfB = b[0] + b[1];

        if (sumOfA >= sumOfB) {
            return a;
        } else {
            return b;
        }

    }

    public int[] makeMiddle(int[] nums) {
        return new int[]{nums[nums.length / 2 - 1], nums[nums.length / 2]};
    }

    public int[] plusTwo(int[] a, int[] b) {
        return new int[]{a[0], a[1], b[0], b[1]};
    }

    public int[] swapEnds(int[] nums) {
        int temp = nums[0];
        nums[0] = nums[nums.length - 1];
        nums[nums.length - 1] = temp;
        return nums;
    }

    public int[] midThree(int[] nums) {
        return new int[]{nums[nums.length / 2 - 1], nums[nums.length / 2], nums[nums.length / 2 + 1]};
    }

    public int maxTriple(int[] nums) {
        int max = nums[0];

        if (max < nums[nums.length - 1]) {
            max = nums[nums.length - 1];
        }

        if (max < nums[nums.length / 2]) {
            max = nums[nums.length / 2];
        }

        return max;
    }

    public int[] frontPiece(int[] nums) {
        if (nums.length < 2) {
            return nums;
        } else {
            return new int[]{nums[0], nums[1]};
        }
    }

    public boolean unlucky1(int[] nums) {
        boolean unlucky1 = false;
        if (nums.length >= 2 && nums[0] == 1 && nums[1] == 3) {
            unlucky1 = true;
        } else if (nums.length > 2 && nums[1] == 1 && nums[2] == 3) {
            unlucky1 = true;
        } else if (nums.length > 3 && nums[nums.length - 2] == 1 && nums[nums.length - 1] == 3) {
            unlucky1 = true;
        }

        return unlucky1;
    }

    public int[] make2(int[] a, int[] b) {
        int[] arrry = new int[2];
        int i = 0;
        int j = 0;
        while (j < a.length && i < arrry.length) {
            arrry[i] = a[j];
            j++;
            i++;
        }
        j = 0;
        while (j < b.length && i < arrry.length) {
            arrry[i] = b[j];
            i++;
            j++;
        }
        return arrry;
    }

    public int[] front11(int[] a, int[] b) {
        if(a.length>0 && b.length>0){
            return new int[]{a[0], b[0]};
        }else if(a.length==0 && b.length>0){
            return new int[]{b[0]};
        }else if (a.length>0 && b.length==0){
            return new int[]{a[0]};
        }else{
            return new int[]{};
        }
    }


}



