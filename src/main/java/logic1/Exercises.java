package logic1;

public class Exercises {

    public static void main(String[] args) {
        System.out.println(22 % 11);
        System.out.println(23 % 11);

        Exercises exercises = new Exercises();
        System.out.println(exercises.nearTen(12));

        System.out.println(exercises.blueTicket(15, 0, 5) );

    }

    public boolean cigarParty(int cigars, boolean isWeekend) {
        boolean result = false;
        if (isWeekend) {
            if (cigars >= 40) {
                result = true;
            }
        } else {
            if (cigars >= 40 && cigars <= 60) {
                result = true;
            }
        }
        return result;
    }

    public int dateFashion(int you, int date) {
        if (you > 2 && date > 2 && (you >= 8 || date >= 8)) {
            return 2;
        } else if (you <= 2 || date <= 2) {
            return 0;
        } else {
            return 1;
        }
    }

    public boolean squirrelPlay(int temp, boolean isSummer) {
        boolean result = false;
        if (isSummer) {
            if (temp >= 60 && temp <= 100) {
                result = true;
            }
        } else {
            if (temp >= 60 && temp <= 90) {
                result = true;
            }
        }
        return result;
    }

    public int caughtSpeeding(int speed, boolean isBirthday) {
        int noTicket = 60;
        int smallTicket = 80;
        if (isBirthday) {
            if (speed <= noTicket + 5) {
                return 0;
            } else if (speed <= smallTicket + 5) {
                return 1;
            } else {
                return 2;
            }
        } else {
            if (speed <= noTicket) {
                return 0;
            } else if (speed <= smallTicket) {
                return 1;
            } else {
                return 2;
            }
        }
    }

    public int sortaSum(int a, int b) {
        int sum = a + b;
        if (sum >= 10 && sum < 20) {
            return 20;
        } else {
            return sum;
        }
    }

    public String alarmClock(int day, boolean vacation) {
        if (day == 6 || day == 0) {
            if (vacation) {
                return "off";
            } else {
                return "10:00";
            }
        } else if (vacation) {
            return "10:00";
        } else {
            return "7:00";
        }
    }

    public boolean love6(int a, int b) {
        return a == 6 || b == 6 || a + b == 6 || a - b == 6 || b - a == 6;
    }

    public boolean in1To10(int n, boolean outsideMode) {
        boolean result = false;
        if (outsideMode) {
            if (n <= 1 || n >= 10) {
                result = true;
            }
        } else {
            if (n > 0 && n < 11) {
                result = true;
            }
        }
        return result;
    }

    public boolean specialEleven(int n) {
        return n % 11 == 0 || n % 11 == 1;
    }

    public boolean more20(int n) {
        return n % 20 == 1 || n % 20 == 2;
    }

    public boolean old35(int n) {
        if (n % 3 == 0 && n % 5 != 0) {
            return true;
        } else if (n % 3 != 0 && n % 5 == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean less20(int n) {
        return n % 20 == 18 || n % 20 == 19;
    }

    public boolean nearTen(int num) {
        return Math.abs(num % 10) <= 2 || num % 10 <= 2;
    }


    public int teenSum(int a, int b) {
        if ((a >= 13 && a <= 19) || (b >= 13 && b <= 19)) {
            return 19;
        } else {
            return a + b;
        }
    }

    public boolean answerCell(boolean isMorning, boolean isMom, boolean isAsleep) {
        if (isAsleep) {
            return false;
        } else if (isMorning) {
            if (isMom) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public int teaParty(int tea, int candy) {
        if (tea < 5 || candy < 5) {
            return 0;
        } else if (tea < candy && tea * 2 <= candy) {
            return 2;
        } else if (candy < tea && candy * 2 <= tea) {
            return 2;
        } else {
            return 1;
        }
    }

    public String fizzString(String str) {
        if (str.startsWith("f") && str.endsWith("b")) {
            return "FizzBuzz";
        } else if (str.startsWith("f")) {
            return "Fizz";
        } else if (str.endsWith("b")) {
            return "Buzz";
        } else {
            return str;
        }
    }

    public String fizzString2(int n) {
        if (n % 3 == 0 && n % 5 == 0) {
            return "FizzBuzz!";
        } else if (n % 3 == 0) {
            return "Fizz!";
        } else if (n % 5 == 0) {
            return "Buzz!";
        } else {
            return n + "!";
        }
    }

    public boolean twoAsOne(int a, int b, int c) {
        return a + b == c || a + c == b || b + c == a;
    }

    public boolean inOrder(int a, int b, int c, boolean bOk) {
        boolean result = false;
        if (bOk) {
            if (c > b) {
                result = true;
            }
        } else {
            if (a < b && b < c) {
                result = true;
            }
        }
        return result;
    }

    public boolean inOrderEqual(int a, int b, int c, boolean equalOk) {
        boolean result = false;
        if (equalOk) {
            if (a <= b && b <= c) {
                result = true;
            }
        } else {
            if (a < b && b < c) {
                result = true;
            }
        }
        return result;
    }


    public boolean lastDigit(int a, int b, int c) {
        int rightmostA = a % 10;
        int rightmostB = b % 10;
        int rightmostC = c % 10;

        return rightmostA == rightmostB || rightmostB == rightmostC || rightmostA == rightmostC;

    }

    public boolean lessBy10(int a, int b, int c) {
        int max = Math.max(a, b);
        max = Math.max(max, c);

        return max - a >= 10 || max - b >= 10 || max - c >= 10;
    }

    public int withoutDoubles(int die1, int die2, boolean noDoubles) {
        if (noDoubles) {
            if (die1 == die2) {
                if (die1 == 6) {
                    die1 = 1;
                } else {
                    die1++;
                }
            }
        }
        return die1 + die2;
    }

    public int maxMod5(int a, int b) {
        if (a == b) {
            return 0;
        } else if (a % 5 == b % 5) {
            if (a < b) {
                return a;
            } else {
                return b;
            }
        } else {
            if (a < b) {
                return b;
            } else {
                return a;
            }
        }
    }


    public int redTicket(int a, int b, int c) {
        if (a == 2 && b == 2 && c == 2) {
            return 10;
        } else if (a == b && b == c) {
            return 5;
        } else if (a != b && a != c) {
            return 1;
        } else {
            return 0;
        }
    }

    public int greenTicket(int a, int b, int c) {
        if (a == b && b == c) {
            return 20;
        }
        if (a == b || a == c || b == c) {
            return 10;
        } else {
            return 0;
        }
    }

    public int blueTicket(int a, int b, int c) {
        int ab = a + b;
        int bc = b + c;
        int ac = a + c;

        if(ab==10 || bc ==10 || ac==10){
            return 10;
        }else if(ab == (bc +10) || ab ==(ac +10)){
            return 5;
        }else {
            return 0;
        }
    }

    public boolean shareDigit(int a, int b) {
        int firstLeft = a/10;
        int firstRight = a%10;
        int secondLeft = b/10;
        int secondRight = b%10;

        return firstLeft ==secondLeft || firstLeft == secondRight || firstRight == secondLeft || firstRight == secondRight;
    }

    public int sumLimit(int a, int b) {
        int maxSumLength = String.valueOf(a).length();
        int sumAb = a+b;
        int actualSumLength = String.valueOf(sumAb).length();

        if(actualSumLength<=maxSumLength){
            return sumAb;
        }else {
            return a;
        }

    }




}
