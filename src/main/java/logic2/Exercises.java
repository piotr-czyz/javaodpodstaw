package logic2;

import java.util.Arrays;
import java.util.Collections;

public class Exercises {

    public static void main(String[] args) {

        Exercises exercises = new Exercises();
        System.out.println(exercises.makeBricks(3, 1, 8));
        System.out.println(exercises.makeBricks(1, 4, 11));

        System.out.println(exercises.noTeenSum(2, 1, 15));
        System.out.println(exercises.roundSum(158, 4, 2));


    }

    public boolean makeBricks(int small, int big, int goal) {
        int actualState = 0;
        int smallIterator = 0;
        int bigIterator;
        boolean result = false;
        while (smallIterator <= small && !result) {
            bigIterator = 0;
            actualState = smallIterator;
            while (bigIterator < big && actualState <= goal && !result) {
                if (actualState == goal) {
                    result = true;
                } else {
                    actualState += 5;
                    bigIterator++;
                }
            }
            if (actualState == goal) {
                result = true;
            } else {
                smallIterator++;
            }
        }
        return result;
    }


    public boolean makeBricks2(int small, int big, int goal) {
        boolean result = true;

        if (big * 5 + small < goal) {
            result = false;
        }
        if (goal % 5 > small) {
            result = false;
        }

        return result;
    }

    public int loneSum(int a, int b, int c) {
        if (a == b && b == c) {
            return 0;
        } else if (a == b && a != c) {
            return c;
        } else if (a != b && b == c) {
            return a;
        } else if (a == c && b != c) {
            return b;
        } else {
            return a + b + c;
        }
    }

    public int luckySum(int a, int b, int c) {
        int sum = 0;
        if (a != 13) {
            sum += a;
            if (b != 13) {
                sum += b;
                if (c != 13) {
                    sum += c;
                }
            }
        }
        return sum;
    }

    public int noTeenSum(int a, int b, int c) {
        return fixTeen(a) + fixTeen(b) + fixTeen(c);
    }

    public int fixTeen(int n) {
        int fixed = n;
        if (n >= 13 && n <= 19) {
            if (n != 15 && n != 16) {
                fixed = 0;
            }
        }
        return fixed;
    }


    public int roundSum(int a, int b, int c) {
        return round10(a) + round10(b) + round10(c);
    }

    public int round10(int num) {
        int rightmostDigit = num % 10;
        int leftDigits = num / 10;

        if (rightmostDigit < 5) {
            return leftDigits * 10;
        } else {
            return (leftDigits + 1) * 10;
        }
    }

    public boolean closeFar(int a, int b, int c) {
        int distanceAb = Math.abs(a - b);
        int distanceAc = Math.abs(a - c);
        int distanceBc = Math.abs(b - c);
        int close = 1;
        int far = 2;

        if (distanceAb <= close && distanceAc >= far && distanceBc >= far) {
            return true;
        } else if (distanceAc <= close && distanceAb >= far && distanceBc >= far) {
            return true;
        } else if (distanceBc <= close && distanceAb >= far && distanceAc >= far) {
            return true;
        } else {
            return false;
        }
    }

    public int blackjack(int a, int b) {
        int distanceForA = 21 - a;
        int distanceForB = 21 - b;

        if (distanceForA < 0 && distanceForB >= 0) {
            return b;
        } else if (distanceForA >= 0 && distanceForB < 0) {
            return a;
        } else if (distanceForA >= 0 && distanceForA < distanceForB) {
            return a;
        } else if (distanceForB >= 0 && distanceForB <= distanceForA) {
            return b;
        } else {
            return 0;
        }
    }

    public boolean evenlySpaced(int a, int b, int c) {
        int[] tab = {a, b, c};
        Arrays.sort(tab);
        return tab[2] - tab[1] == tab[1] - tab[0];
    }

    public int makeChocolate(int small, int big, int goal) {

        if (small + (5 * big) < goal || goal % 5 > small) {
            return -1;
        } else {
            int bigIterator = 0;
            int actualBigValue = 0;
            while (bigIterator < big && actualBigValue + 5 <= goal) {
                actualBigValue += 5;
                bigIterator++;
            }

            return goal - actualBigValue;
        }
    }




}
