package string2;

public class Exercises {

    public String doubleChar(String str) {
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            result = result + str.charAt(i) + str.charAt(i);
        }
        return result;
    }

    public int countHi(String str) {
        int counter = 0;
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.substring(i).startsWith("hi")) {
                counter++;
            }
        }

        return counter;
    }

    public boolean catDog(String str) {
        int catCounter = 0;
        int dogCounter = 0;

        for (int i = 0; i < str.length() - 2; i++) {
            if (str.substring(i).startsWith("cat")) {
                catCounter++;
            }
            if (str.substring(i).startsWith("dog")) {
                dogCounter++;
            }
        }
        return catCounter == dogCounter;
    }


    public int countCode(String str) {
        int counter = 0;

        for (int i = 0; i < str.length() - 3; i++) {
            if (str.substring(i).matches("co.e.*")) {
                counter++;
            }
        }
        return counter;
    }


    public boolean endOther(String a, String b) {
        a = a.toLowerCase();
        b = b.toLowerCase();

        if (a.length() < b.length()) {
            return b.endsWith(a);
        } else {
            return a.endsWith(b);
        }
    }

    public boolean xyzThere(String str) {
        return str.matches(".*[^\\.]xyz.*") || str.matches("^xyz.*");
    }

    public boolean bobThere(String str) {
        return str.matches(".*b.b.*");
    }

    public boolean xyBalance(String str) {
        return str.lastIndexOf('y') >= str.lastIndexOf('x');
    }

    public String mixString(String a, String b) {
        String result = "";

        if (a.length() <= b.length()) {
            int i;
            for (i = 0; i < a.length(); i++) {
                result = result + a.charAt(i) + b.charAt(i);
            }
            if (b.length() > i)
                result = result + b.substring(i);
        } else {
            int i;
            for (i = 0; i < b.length(); i++) {
                result = result + a.charAt(i) + b.charAt(i);
            }
            if (a.length() > i)
                result = result + a.substring(i);
        }
        return result;
    }

    public String repeatEnd(String str, int n) {
        String result = "";
        for (int i = 0; i < n; i++) {
            result = result + str.substring(str.length() - n);
        }
        return result;
    }

    public String repeatFront(String str, int n) {
        String result = "";
        for (int i = 0; i < n; i++) {
            result = result + str.substring(0, n - i);
        }
        return result;
    }

    public String repeatSeparator(String word, String sep, int count) {
        String result = "";
        if (count > 0) {
            for (int i = 1; i < count; i++) {
                result += word + sep;
            }
            result += word;
        }
        return result;
    }

    public boolean prefixAgain(String str, int n) {
        String prefix = str.substring(0,n);
        if(str.length()>1){
            return str.substring(1).matches(".*"+prefix+".*");
        }else{
            return false;
        }
    }

    public boolean xyzMiddle(String str) {
        if(str.length()<3){
            return false;
        }else {
            int halfOfStringLength= str.length()/2;
            int xyzExactlyInTheMiddle = halfOfStringLength-1;
            int oneMoreToSide = halfOfStringLength-2;
            return str.matches(".{"+xyzExactlyInTheMiddle+"}xyz.{"+xyzExactlyInTheMiddle+"}") ||
                    str.matches(".{"+xyzExactlyInTheMiddle+"}xyz.{"+oneMoreToSide+"}") ||
                    str.matches(".{"+oneMoreToSide+"}xyz.{"+xyzExactlyInTheMiddle+"}");

        }
    }

    public String getSandwich(String str) {
        int leftBread = str.indexOf("bread");
        int rightBread = str.lastIndexOf("bread");

        if(leftBread<rightBread){
            return str.substring(leftBread+5, rightBread);
        }else {
            return "";
        }
    }

    public boolean sameStarChar(String str) {
        for(int i=1; i<str.length()-1; i++){
            if(str.charAt(i)=='*'){
                if(str.charAt(i-1)!=str.charAt(i+1)){
                    return false;
                }
            }
        }
        return true;
    }

    public String oneTwo(String str) {
        String result ="";
        for(int i=0; i<str.length(); i=i+3){
            if(i+2<str.length()){
                result += str.substring(i+1, i+3) + str.charAt(i);
            }
        }
        return result;
    }

}
